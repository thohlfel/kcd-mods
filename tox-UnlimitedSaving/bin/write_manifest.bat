@echo off
setlocal enableextensions enabledelayedexpansion
chcp 65001 >nul

echo.[INFO] Checking pre-conditions...
set "SKEL_FILE=%~dp0manifest_skeleton.txt"
if not exist "%SKEL_FILE%" goto :error-skeleton-missing
for %%a in ("%~dp0\..") do set "PACKAGE_NAME=%%~nxa"

set "MY_TIME=%TIME: =0%"
set "MY_DATE=%DATE% %MY_TIME:~0,8%"
echo.[INFO] Using timestamp: %MY_DATE% 

set "OUT_DIR=%~1"
if not defined OUT_DIR (
	set "OUT_DIR=%~dp0build\"
	echo.[WARN] No Build directory defined as parameter - use default build dir...
)
echo.[INFO] Using output dir: [%OUT_DIR%]

set "MANIFEST_FILE=%OUT_DIR%mod.manifest"
if exist "%MANIFEST_FILE%" goto :error-manifest-exists

pushd "%~dp0"
for /f %%x in (..\src\version.txt) do set CURRENT_VERSION=%%x
for /f %%x in (..\src\kcd-version.txt) do set KCD_VERSION=%%x

if not defined CURRENT_VERSION (
	echo.[WARN] --- No version.txt file found - set to [1.0]
	set "CURRENT_VERSION=1.0"
) else (
	echo.[INFO] Current version is: [%CURRENT_VERSION%]
)

for /f "tokens=*" %%a in (.\manifest_skeleton.txt) do (
	
	set "MY_LINE=%%a"
	set "MY_LINE=!MY_LINE:_package_=%PACKAGE_NAME%!"
	set "MY_LINE=!MY_LINE:_version_=%CURRENT_VERSION%!"
	set "MY_LINE=!MY_LINE:_kcd-version_=%KCD_VERSION%!"
	set "MY_LINE=!MY_LINE:_date_=%MY_DATE%!"
	echo.!MY_LINE! >>"%MANIFEST_FILE%"
)
popd

echo.[INFO] Manifest-File created successfully [%MANIFEST_FILE%]
exit /b

:error-skeleton-missing
echo.[ERROR] no skeleton file found [%SKEL_FILE%]
pause
exit /b 1

:error-manifest-exists
echo.[ERROR] Manifest file already exists [%MANIFEST_FILE%] 
echo.[HINT] Delete it and re-run me [%~dpnx0]
pause
exit /b 2