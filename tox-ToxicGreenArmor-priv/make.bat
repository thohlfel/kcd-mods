@echo off
setlocal enableextensions enabledelayedexpansion

set "BUILD_ROOT=%~dp0build\"
set "BAK_ROOT=%~dp0bak\"
rem :: gets the package name from folder-name
for %%a in ("%~dp0\.") do set "PACKAGE_NAME=%%~nxa"
rem :: TODO add libs path[s] here - RELATIVE PATHS! delimited by space eg: "LIBS_TO_PAK=.\src\Libs .\src\Objects"
set "LIBS_TO_PAK=.\src\Libs .\src\Objects"

call :backup
call :write-manifest
call :build-mod
call :build-localization
call :build-manual_localization
call :build-delivery-package

pause
endlocal
exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:backup

if exist "%BUILD_ROOT%%PACKAGE_NAME%.zip" (
	call :backup-existing-package "%BUILD_ROOT%%PACKAGE_NAME%.zip"
)
echo.################
echo.Clean-up Build-Dir and Setup building pipeline...
echo.################
echo.
if not exist "%BUILD_ROOT%" (
	mkdir "%BUILD_ROOT%"
) else (
	del /q "%BUILD_ROOT%*"
	FOR /D %%p IN ("%BUILD_ROOT%*.*") DO rmdir "%%p" /s /q
)
set "PACKAGE_ROOT=%BUILD_ROOT%%PACKAGE_NAME%\"
mkdir "%PACKAGE_ROOT%"
exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:backup-existing-package <file_to_backup>
echo.################
echo.Backup existing Package...
echo.################
echo.
if not exist "%~1" (
	echo.[ERROR] Unable to find file: [%~1]
	pause
	exit /b 3
)
set "LAST_MOD_DATE=%~t1"
rem :: filename conventions
set "LAST_MOD_DATE=!LAST_MOD_DATE::=-!"
set "LAST_MOD_DATE=!LAST_MOD_DATE:/=-!"
set "LAST_MOD_DATE=!LAST_MOD_DATE:.=-!"
set "newName=%PACKAGE_NAME% (!LAST_MOD_DATE!).zip"
echo.Backup-Name for last Build: [!newName!]
if not exist "%BAK_ROOT%" mkdir "%BAK_ROOT%"
move ".\build\%PACKAGE_NAME%.zip" "%BAK_ROOT%!newName!"
exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:write-manifest
echo.################
echo.Write manifest file...
echo.################
echo.
call "%~dp0bin\write_manifest.bat" "%PACKAGE_ROOT%"
if NOT %ERRORLEVEL% == 0 goto :error-writing-manifest
exit /b 0

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:build-mod
if not defined LIBS_TO_PAK (
	echo.[33m### NO LIBRARIES TO PACK - skipping sub-task... ###[0m
	echo.[32mPossibly TODO: fill LIBS_TO_PAK variable in [1mmake.bat[0m
	pause
	exit /b
)
echo.
echo.################
echo.Creating Mod-Package from Source...
echo.################
echo.
call 7z.exe a -tzip "%PACKAGE_ROOT%Data\%PACKAGE_NAME%.pak" %LIBS_TO_PAK%
if %errorlevel% NEQ 0 goto :error-exit
exit /b 0

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:build-localization
set "LOCALIZATION_DIR=%PACKAGE_ROOT%Localization\"
if exist ".\src\Localization" (
	echo.################
	echo.Preparing Localization packages...
	echo.################
	echo.
	mkdir "%LOCALIZATION_DIR%"
	if exist ".\src\Localization\de\" (
		echo....writing german localization
		call 7z.exe a -tzip "%LOCALIZATION_DIR%German_xml.pak" ".\src\Localization\de\*"
		echo.done
	)
	if exist ".\src\Localization\en\" (
		echo....writing english localization
		call 7z.exe a -tzip "%LOCALIZATION_DIR%English_xml.pak" ".\src\Localization\en\*"
		echo.done
	)
) else (
	echo.No Localization found for this Mod. Skippping sub-task...
)
exit /b


:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:build-manual_localization
set "MANUAL_LOCALIZATION_NAME=%PACKAGE_NAME%_manual_localization"
set "MANUAL_LOCALIZATION_DIR=%BUILD_ROOT%%MANUAL_LOCALIZATION_NAME%\"
echo.################
echo.Preparing Manual Localization Package...
echo.################
set "LOCALIZATION_XML_FILE=text_ui_items.xml"
set "ENGLISH_PAK_NAME=English_xml.pak"
set "GERMAN_PAK_NAME=German_xml.pak"
set "ORIGINAL_ENGLISH_XML_PAK=%~dp0bin\%ENGLISH_PAK_NAME%"
set "ORIGINAL_GERMAN_XML_PAK=%~dp0bin\%GERMAN_PAK_NAME%"

mkdir "%MANUAL_LOCALIZATION_DIR%"

if exist "%ORIGINAL_ENGLISH_XML_PAK%" (
	echo.Writing english xml pak...
	copy "%ORIGINAL_ENGLISH_XML_PAK%" "%MANUAL_LOCALIZATION_DIR%"
	call 7z.exe a -tzip "%MANUAL_LOCALIZATION_DIR%%ENGLISH_PAK_NAME%" ".\src\Localization\en\%LOCALIZATION_XML_FILE%"
	echo....english pak done
) else (
	echo.[33mUnable to find original localization pak for english: [%ORIGINAL_ENGLISH_XML_PAK%][0m
)
if exist "%ORIGINAL_GERMAN_XML_PAK%" (
	echo.Writing german xml pak...
	copy "%ORIGINAL_GERMAN_XML_PAK%" "%MANUAL_LOCALIZATION_DIR%"
	call 7z.exe a -tzip "%MANUAL_LOCALIZATION_DIR%%GERMAN_PAK_NAME%" ".\src\Localization\de\%LOCALIZATION_XML_FILE%"
	echo....german pak done
) else (
	echo.[33mUnable to find original localization pak for german: [%ORIGINAL_GERMAN_XML_PAK%][0m
)
echo....manual localization packages done

echo.creating delivery package for manual localization
call 7z.exe a -tzip "%BUILD_ROOT%%MANUAL_LOCALIZATION_NAME%.zip" "%MANUAL_LOCALIZATION_DIR%" 

echo....manual localization delivery package done
exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:copy-auto-scripts
echo.################
echo.Copy auto-script[s]...
echo.################
echo.
copy ".\src\*.txt" "%PACKAGE_ROOT%"
exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:build-delivery-package
echo.################
echo.Building delivery package...
echo.################
echo.
call 7z.exe a -tzip "%BUILD_ROOT%%PACKAGE_NAME%.zip" "%PACKAGE_ROOT%"
echo.
echo.################
echo.[32mProcess Done Successfully....[0m

exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:error-exit
echo.################
echo.[33mTo run this script "7z packer" is required and set-up path to "7z.exe" in your PATH environment variable.[0m
echo.################
echo.
pause

exit /b

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:error-writing-manifest
echo.################
echo.[33mSomething goes wrong on writing manifest file [check cmd output above...].[0m
echo.################
echo.
pause

exit /b
