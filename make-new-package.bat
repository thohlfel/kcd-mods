@echo off
echo.##############################
echo.[32mCreating new KCD-Mod package from base-package...[0m
echo.##############################
echo.

setlocal enableextensions disabledelayedexpansion
:askFile

rem Retrieve filename. On empty input ask again
set /p "my_file=Enter filename for new KCD-Mod package: " || goto :askFile

rem :: first replace space with underscore to avoid path problems in resulting mod
set "my_file=%my_file: =_%"

rem See Naming Files, Paths, and Namespaces
rem     https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247%28v=vs.85%29.aspx


rem NOTE: From here, we will be enabling/disabling delayed expansion 
rem       to avoid problems with special characters
setlocal enabledelayedexpansion
rem Ensure we do not have restricted characters in file name trying to use them as 
rem delimiters and requesting the second token in the line
for /f tokens^=2^ delims^=^<^>^:^"^/^\^|^?^*^ eol^= %%y in ("[!my_file!]") do (
	rem If we are here there is a second token, so, there is a special character
	call :error "Non allowed characters in file name"
	endlocal & goto :askFile
)

rem Check MAX_PATH (260) limitation
set "my_temp_file=!cd!\!my_file!" & if not "!my_temp_file:~260!"=="" (
	call :error "File name too long"
	endlocal & goto :askFile
)

rem Check path inclusion, file name correction
for /f delims^=^ eol^= %%a in ("!my_file!") do (
	rem Cancel delayed expansion to avoid ! removal during expansion
	endlocal

	rem Until checked, we don't have a valid file
	set "my_file="

	rem Check we don't have a path 
	if /i not "%%~a"=="%%~nxa" (
		call :error "Paths are not allowed - enter a package name"
		goto :askFile
	)

	rem Check it is not an existing folder 
	if exist "%%~nxa\" (
		call :error "Already a Mod with same name exists - choose another name!"
		echo.Existing Mods:
		dir /a:d /b
		goto :askFile
	)

	rem ASCII 0-31 check. Check file name can be created
	2>nul ( >>"%%~nxa" type nul ) || (
		call :error "File name is not valid for this file system"
		goto :askFile
	)

	rem Ensure it was not a special file name by trying to delete the newly created file
	2>nul ( del /q /f /a "%%~nxa" ) || (
		call :error "Reserved file name used"
		goto :askFile
	)

	rem Everything was OK - We have an allowed folder name 
	set "my_file=%%~nxa\"
)

echo.Creating new Mod Package: "%my_file%"
mkdir "%my_file%"
cd "%my_file%"
echo cleaning up base-packege befory copying...
if exist "..\base-package\bak\" (rd /s "..\base-package\bak\")
if exist "..\base-package\build\" (rd /s "..\base-package\build\")
xcopy "..\base-package\*" /e
echo.%my_file% >>sub-projects.lst
echo done...
pause
goto :eof

:error <err-message>
echo.[33m[ERROR]::[0m %~1%2%3%4%5%6%7%8%9
exit /b